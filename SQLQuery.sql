use [med.id]

--
truncate table m_user
select * from m_user
insert into m_user values	
(1, 1, 'admin@gmail.com', 1234, 0,0, getdate(), 1, getdate(), null, null, null, null,0),
(2, 2, 'dokter@gmail.com', 1234, 0,0, getdate(), 1, getdate(), null, null, null, null,0),
(3, 3, 'faskes@gmail.com', 1234, 0,0, getdate(), 1, getdate(), null, null, null, null,0),
(4, 4, 'pasien@gmail.com', 1234, 0,0, getdate(), 1, getdate(), null, null, null, null,0),
(5, 4, 'elga@gmail.com', 1234, 0,0, getdate(), 1, getdate(), null, null, null, null,0)
--
select * from m_role

select * from m_menu
select * from m_menu_role


--
--truncate table m_menu
select * from m_menu


truncate table m_menu
insert into m_menu values
('Master', '', 0, 'nav-icon fas fa-light', 'nav-icon fas fa-light' ,1, getdate(), null, null, null, null, 0),
('Transaction', '', 0, 'nav-icon fas fa-light', 'nav-icon fas fa-light' ,1, getdate(), null, null, null, null, 0),
('Home', 'Home', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('Admin', 'Admin', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon', 1, getdate(), null, null, null, null, 0),
('Bank', 'Bank', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('Biodata', 'Biodata', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('BiodataAddress' , 'BiodataAddress', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('BloodGroup', 'BloodGroup', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('Courier', 'Courier', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('CourierType', 'CourierType', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('CourierDiscount' , 'CourierDiscount', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('Customer', 'Customer', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('CustomerMember', 'CustomerMember', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('CustomerRelation' , 'CustomerRelation', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('CustomerChat', 'CustomerChat', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' , 1, getdate(), null, null, null, null, 0),
('CustomerChatHistory','CustomerChatHistory', 1,'far fa-light fa-file nav-icon','far fa-light fa-file nav-icon',1, getdate(), null, null, null, null,0),
('CustCustNominal', 'CustCustNominal', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('CustomerRegisCard', 'CustomerRegisCard', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('CustomerVA', 'CustomerVA', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('CustomerVAHist', 'CustomerVAHist', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon', 1, getdate(), null, null, null, null, 0),
('CustomerWallet', 'CustomerWallet', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('CustomerWallTP', 'CustomerWallTP', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('CustomerWallWD', 'CustomerWallWD', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('Doctor','Doctor', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon', 1, getdate(), null, null, null, null, 0),
('DoctorEducation', 'DoctorEducation', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('DoctorTreatment', 'DoctorTreatment', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('DoctorOffice', 'DoctorOffice', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('DoctorOfficeSche', 'DoctorOfficeSche', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon', 1, getdate(), null, null, null, null, 0),
('DoctorOfficeTreat', 'DoctorOfficeTreat', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('DoctorOfcTreatPrice', 'DoctorOfcTreatPrice', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('CurrentDoctorSpez', 'CurrentDoctorSpez', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('EducationLevel' , 'EducationLevel', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('Location', 'Location', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('LocationLevel' , 'LocationLevel', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('MedicalFacility', 'MedicalFacility', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('MedFacilityCatg', 'MedFacilityCatg', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('MedFacilitySch', 'MedFacilitySch', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('MedicalItem', 'MedicalItem', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('MedItemCatg', 'MedItemCatg', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('MedItemSegmen', 'MedItemSegmen', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('MedlItemPurch', 'MedlItemPurch', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('MedItemPurchDet', 'MedItemPurchDet', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('Menu', 'Menu', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('MenuRole', 'MenuRole', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('PaymentMethod', 'PaymentMethod', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('Role', 'Role', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('Spezialization', 'Spezialization', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('User', 'User', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('Wallet', 'Wallet', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('BioAttachment', 'BioAttachment', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('Appointment', 'Appointment', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('AppointmentResch', 'AppointmentResch', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0),
('TreatmentDisc' , 'TreatmentDisc', 1, 'far fa-light fa-file nav-icon', 'far fa-light fa-file nav-icon' ,1, getdate(), null, null, null, null, 0)
--

update m_menu set url = 'Index' where Id > 2

select * from m_menu as parent
join m_menu_role as mr on parent.Id = mr.menu_Id
where mr.role_Id = 1 and parent.parent_id = 0

select * from m_menu as parent
join m_menu_role as mr on parent.Id = mr.menu_Id
where parent.parent_id = 0 and parent.is_delete=0 and mr.is_delete=0 and mr.role_Id=2


select * from m_menu as parent
join m_menu_role as mr on parent.Id = mr.menu_Id
where parent.parent_id = 1 and parent.is_delete=0 and mr.is_delete=0 and mr.role_Id=2


--truncate table m_menu_role
select * from m_menu_role
select * from m_menu
select * from m_role

truncate table m_menu_role
insert into m_menu_role values
-- Menu Admin
(1,1, 1, getdate(), null, null, null, null, 0), -- Menu Home/ Landingpage awal
(2,1, 1, getdate(), null, null, null, null, 0), -- Menu Admin
(3,1, 1, getdate(), null, null, null, null, 0), -- Menu Bank
(4,1, 1, getdate(), null, null, null, null, 0), -- Menu Biodata/ Profil
(5,1, 1, getdate(), null, null, null, null, 0), -- Menu Biodata/ Profil
(6,1, 1, getdate(), null, null, null, null, 0), -- Menu
(7,1, 1, getdate(), null, null, null, null, 0), -- Menu
(8,1, 1, getdate(), null, null, null, null, 0), -- Menu
(9,1, 1, getdate(), null, null, null, null, 0), -- Menu
(10,1, 1, getdate(), null, null, null, null, 0), -- Menu
(11,1, 1, getdate(), null, null, null, null, 0), -- Menu
(12,1, 1, getdate(), null, null, null, null, 0), -- Menu
(13,1, 1, getdate(), null, null, null, null, 0), -- Menu
(14,1, 1, getdate(), null, null, null, null, 0), -- Menu
(15,1, 1, getdate(), null, null, null, null, 0), -- Menu
(16,1, 1, getdate(), null, null, null, null, 0), -- Menu
(17,1, 1, getdate(), null, null, null, null, 0), -- Menu
(18,1, 1, getdate(), null, null, null, null, 0), -- Menu
(19,1, 1, getdate(), null, null, null, null, 0), -- Menu
(20,1, 1, getdate(), null, null, null, null, 0), -- Menu
(21,1, 1, getdate(), null, null, null, null, 0), -- Menu
(22,1, 1, getdate(), null, null, null, null, 0), -- Menu
(23,1, 1, getdate(), null, null, null, null, 0), -- Menu
(24,1, 1, getdate(), null, null, null, null, 0), -- Menu
(25,1, 1, getdate(), null, null, null, null, 0), -- Menu
(26,1, 1, getdate(), null, null, null, null, 0), -- Menu
(27,1, 1, getdate(), null, null, null, null, 0), -- Menu
(28,1, 1, getdate(), null, null, null, null, 0), -- Menu
(29,1, 1, getdate(), null, null, null, null, 0), -- Menu
(30,1, 1, getdate(), null, null, null, null, 0), -- Menu
(31,1, 1, getdate(), null, null, null, null, 0), -- Menu
(32,1, 1, getdate(), null, null, null, null, 0), -- Menu
(33,1, 1, getdate(), null, null, null, null, 0), -- Menu
(34,1, 1, getdate(), null, null, null, null, 0), -- Menu
(35,1, 1, getdate(), null, null, null, null, 0), -- Menu
(36,1, 1, getdate(), null, null, null, null, 0), -- Menu
(37,1, 1, getdate(), null, null, null, null, 0), -- Menu
(38,1, 1, getdate(), null, null, null, null, 0), -- Menu
(39,1, 1, getdate(), null, null, null, null, 0), -- Menu
(40,1, 1, getdate(), null, null, null, null, 0), -- Menu
(41,1, 1, getdate(), null, null, null, null, 0), -- Menu
(42,1, 1, getdate(), null, null, null, null, 0), -- Menu
(43,1, 1, getdate(), null, null, null, null, 0), -- Menu
(44,1, 1, getdate(), null, null, null, null, 0), -- Menu
(45,1, 1, getdate(), null, null, null, null, 0), -- Menu
(46,1, 1, getdate(), null, null, null, null, 0), -- Menu
(47,1, 1, getdate(), null, null, null, null, 0), -- Menu
(48,1, 1, getdate(), null, null, null, null, 0), -- Menu
(49,1, 1, getdate(), null, null, null, null, 0), -- Menu
(50,1, 1, getdate(), null, null, null, null, 0), -- Menu
(51,1, 1, getdate(), null, null, null, null, 0), -- Menu
(52,1, 1, getdate(), null, null, null, null, 0), -- Menu
(53,1, 1, getdate(), null, null, null, null, 0), -- Menu

--Menu Doctor
(1,2, 1, getdate(), null, null, null, null, 0),
(3,2, 1, getdate(), null, null, null, null, 0), -- Menu Landing Page/ home
(6,2, 1, getdate(), null, null, null, null, 0), -- Menu Profil/ Biodata
(24,2, 1, getdate(), null, null, null, null, 0), -- Menu Dokter
(31,2, 1, getdate(), null, null, null, null, 0), -- Menu Tambah Dokter Spesialis
(47,2, 1, getdate(), null, null, null, null, 0), -- Menu Spesialis Dokter

--Menu Faskes
(6,3, 1, getdate(), null, null, null, null, 0), -- Menu Profil/ Biodata
(3,3, 1, getdate(), null, null, null, null, 0), -- Menu Landing Page/ home
(9,3, 1, getdate(), null, null, null, null, 0), -- Menu Courier
(10,3, 1, getdate(), null, null, null, null, 0), -- Menu Courier
(1,3, 1, getdate(), null, null, null, null, 0), -- Menu


--Menu Pasien
(1,4, 1, getdate(), null, null, null, null, 0), -- Menu
(3,4, 1, getdate(), null, null, null, null, 0), -- Menu Landing Page/ home
(5,4, 1, getdate(), null, null, null, null, 0), -- Menu Biodata/ Profil
(6,4, 1, getdate(), null, null, null, null, 0), -- Menu Profil/ Biodata
(7,4, 1, getdate(), null, null, null, null, 0), -- Menu Tab Alamat
(12,4, 1, getdate(), null, null, null, null, 0), -- Menu Pasien
(24,4, 1, getdate(), null, null, null, null, 0), -- Menu Cari Dokter/ Detail
(37,4, 1, getdate(), null, null, null, null, 0), -- Menu Riwayat Kedatangan
(38,4, 1, getdate(), null, null, null, null, 0), -- Menu Produk  Kesehatan
(50,4, 1, getdate(), null, null, null, null, 0), -- Menu Tab Profil
(51,4, 1, getdate(), null, null, null, null, 0) -- Menu Buat Janji




-- Menu Umum 
--(3,5, getdate(), 1, getdate(), 1, getdate(), 0)
--(24,5, getdate(), 1, getdate(), 1, getdate(), 0)


select * from m_biodata
truncate table m_biodata
insert into m_biodata values	
('dr. Tatjana Saphira, Sp.A', '081234567890', null,null, 1, getdate(), null, null, null, null,0),
('dr. Alwi Fadli, Sr.G', '081234567890', null,null, 1, getdate(), null, null, null, null,0),
('dr. Toni Agustina, Sp.Farm', '081234567890', null,null, 1, getdate(), null, null, null, null,0)

truncate table m_doctor
select * from m_doctor
insert into m_doctor values	
(1, 'DR001', 1, getdate(), null, null, null, null,0),
(2, 'DR002', 1, getdate(), null, null, null, null,0),
(3, 'DR003', 1, getdate(), null, null, null, null,0)

select * from m_biodata
-- Join Table JadwaL Praktek
select * from m_medical_facility -- RS Fasilitas -- Lokasi Praktek
select * from m_medical_facility_schedule -- Jadwal
select * from m_location -- Lokasi 
select * from m_medical_facility_category -- Jenis Poli
select * from t_doctor_office_treatment_price -- Biaya treatmen
select * from t_doctor_office_schedule -- Schedule


-- Join Table Riwayat Praktek
select * from m_medical_facility -- RS Fasilitas -- Lokasi Praktek
select * from m_location -- Lokasi 
select * from m_specialization -- Spesilis 
select * from m_doctor
select * from t_current_doctor_specialization

--Quwery
select mdr.name as Name_RS, loc.name as Name_Loc
from m_medical_facility as mdr
join m_location as loc on mdr.location_id = loc.id

--Quwery Tindakan Medis Doctor
select bio.fullname as NamaDoctor, tdm.name as TindakanMedis
from t_doctor_treatment as tdm
join m_doctor as doc on doc.id = tdm.doctor_id
join m_biodata as bio on doc.biodata_id = bio.id

-- Select From
select * from t_doctor_treatment
select * from m_doctor
select * from m_biodata

-- Truncate Table
truncate table t_doctor_treatment
truncate table m_doctor
truncate table m_doctor

-- Isi table
--Table Treatmen Doctor
truncate table t_doctor_treatment
Insert into t_doctor_treatment values
(1, 'Fisioterapi Anak',1, GETDATE(), null, null, null, null, 0),
(1, 'Konsultasi Kesehatan Anak',1, GETDATE(), null, null, null, null, 0),
(1, 'Skrining Tumbuh Kembang Anak',1, GETDATE(), null, null, null, null, 0),
(1, 'Vaksin Anak',1, GETDATE(), null, null, null, null, 0),
(1, 'Konsultasi Alergi Anak',1, GETDATE(), null, null, null, null, 0),
(1, 'Konsultasi Psikologi Anak',1, GETDATE(), null, null, null, null, 0),
(1, 'Tes Pendengaran OAE',1, GETDATE(), null, null, null, null, 0),
(2, 'Ahli Gizi',1, GETDATE(), null, null, null, null, 0),
(2, 'Ahli Diet',1, GETDATE(), null, null, null, null, 0),
(2, 'Ahli Pola Hidup Sehat',1, GETDATE(), null, null, null, null, 0),
(2, 'Ahli Kecantikan',1, GETDATE(), null, null, null, null, 0),
(2, 'Ahli Makanan',1, GETDATE(), null, null, null, null, 0),
(3, 'Ahli Farmasi Obat',1, GETDATE(), null, null, null, null, 0),
(3, 'Spesialis Obat',1, GETDATE(), null, null, null, null, 0),
(3, 'Spesialis Penyakit Dalam',1, GETDATE(), null, null, null, null, 0),
(4, 'Cabut Gigi', 1, GETDATE(), null, null, null, null, 0),
(3, 'Imunisasi',1, GETDATE(), null, null, null, null, 0)

--Quwery Pendidikan Doctor
select bio.fullname as NamaDoctor, edu.institution_name as Pendidikan, edu.major as Jurusan, edu.end_year as TahunSelesai
from m_doctor_education as edu
join m_doctor as doc on doc.id = edu.doctor_id
join m_biodata as bio on doc.biodata_id = bio.id

-- Select From
select * from m_doctor_education
select * from m_doctor
select * from m_biodata

-- Truncate Table
truncate table m_doctor_education
truncate table m_doctor
truncate table m_doctor

-- Isi table
--Isi table Treatmen Doctor
Insert into m_doctor_education values
(1, 1, 'Universitas Padjadjaran', 'Spesialis Anak', 2007, 2011, 0, 1, GETDATE(), null, null, null, null, 0),
(1, 2, 'Universitas Padjadjaran', 'Kedokteran Umum', 2012, 2016, 1, 1, GETDATE(), null, null, null, null, 0),
(2, 1, 'Universitas Indonesia', 'Ahli Gizi', 2014, 2018, 0, 1, GETDATE(), null, null, null, null, 0),
(2, 2, 'Universitas Indonesia', 'Spesialis Gizi', 2018, 2021, 1, 1, GETDATE(), null, null, null, null, 0),
(3, 1, 'Institut Teknologi Bandung', 'Ahli Kecantikan', 2014, 2018, 0, 1, GETDATE(), null, null, null, null, 0),
(3, 2, 'Institut Teknologi Bandung', 'Spesialis Kecantikan', 2018, 2021, 1, 1, GETDATE(), null, null, null, null, 0)


-- Query Riwayat Praktek Doctor
select bio.fullname as NamaDoctor, spe.name as NamaSpesialis, mdfc.name as NamaRS, loc.name as LocName
from t_current_doctor_specialization as dsp
join m_specialization as spe on spe.Id = dsp.specialization_id
join m_doctor as doc on doc.Id = dsp.doctor_id
join m_biodata as bio on doc.biodata_id = bio.id
join t_doctor_office_schedule as drsc on doc.id = drsc.doctor_id
join m_medical_facility_schedule as mdfs on drsc.medical_facility_schedule_id = mdfs.id
join m_medical_facility as mdfc on mdfs.medical_facility_id = mdfc.id
join m_location as loc on mdfc.location_id = loc.id

-- Select From
select * from t_current_doctor_specialization
select * from m_specialization
select * from m_doctor
select * from m_biodata
select * from t_doctor_office_schedule
select * from m_medical_facility_schedule
select * from m_medical_facility
select * from m_medical_facility_category
select * from m_location
select * from m_location_level

-- Truncate Table
truncate table t_current_doctor_specialization
truncate table m_specialization
truncate table t_doctor_office_schedule
truncate table m_medical_facility_schedule
truncate table m_medical_facility
truncate table m_location
truncate table m_medical_facility_category

-- Isi table
--Table 
select * from t_current_doctor_specialization
Insert into t_current_doctor_specialization values
(1, 1, 1, GETDATE(), null, null, null, null, 0),
(2, 2, 1, GETDATE(), null, null, null, null, 0),
(3, 3, 1, GETDATE(), null, null, null, null, 0)

--Table
select * from m_specialization

Insert into m_specialization values
('Spesialis Anak', 1, GETDATE(), null, null, null, null, 0),
('Spesialis Gizi', 1, GETDATE(), null, null, null, null, 0),
('Spesialis Kulit', 1, GETDATE(), null, null, null, null, 0)

--Table
select * from m_location
Insert into m_location values
('DKI Jakarta', 0, 1, 1, GETDATE(), null, null, null, null, 0),
('Jawa Barat', 0, 1, 1, GETDATE(), null, null, null, null, 0),
('JakPus', 1, 2, 1, GETDATE(), null, null, null, null, 0),
('JakUt', 1, 2, 1, GETDATE(), null, null, null, null, 0),
('JakSel', 1, 2, 1, GETDATE(), null, null, null, null, 0),
('JakBar', 1, 2, 1, GETDATE(), null, null, null, null, 0),
('JakTim', 1, 2, 1, GETDATE(), null, null, null, null, 0),
('Bandung', 1, 2, 1, GETDATE(), null, null, null, null, 0),
('Depok', 1, 2, 1, GETDATE(), null, null, null, null, 0)

--Table
select * from m_medical_facility_category
Insert into m_medical_facility_category values
('Poliklinik Anak', 1, GETDATE(), null, null, null, null, 0),
('Klinik', 1, GETDATE(), null, null, null, null, 0),
('Rumah Sakit', 1, GETDATE(), null, null, null, null, 0),
('Puskesmas', 1, GETDATE(), null, null, null, null, 0)

drop table [m_medical_facility]
CREATE TABLE [dbo].[m_medical_facility](
	[id] [bigint] primary key IDENTITY(1,1) NOT NULL,
	[name] [varchar](50) NULL,
	[medical_facility_category_id] [bigint] NULL,
	[location_id] [bigint] NULL,
	[full_address] [varchar] (max) NULL,
	[email] [varchar](100) NULL,
	[phone_code] [varchar](10) NULL,
	[phone] [varchar](15) NULL,
	[fax] [varchar](15) NULL,
	[created_by] [bigint] NOT NULL,
	[created_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[deleted_by] [bigint] NULL,
	[delted_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
)

--Table
truncate table m_medical_facility
select * from m_medical_facility
Insert into m_medical_facility values
('RS Mitra', 1, 9, 'Jl. Kebenaaran Kav 3S', 'RSMitra@gmail.com', 0712345678, 081234567890, 987654, 1, GETDATE(), null, null, null, null, 0),
('RSIA Bunda', 1, 3, 'Jl.Kesabaran no 932', 'RSIABunda@gmail.com', 0712345678, 081234567890, 987654, 1, GETDATE(), null, null, null, null, 0),
('RSUP Hasan Sadikin', 1, 8, 'Jl.Hasan Sadikin', 'RSUPHasanSadikin@gmail.com', 0712345678, 081234567890, 987654, 1, GETDATE(), null, null, null, null, 0)

select * from t_doctor_office_schedule
--Table
Insert into t_doctor_office_schedule values
(1, 1, 10, 1, GETDATE(), null, null, null, null, 0),
(1, 2, 10, 1, GETDATE(), null, null, null, null, 0),
(1, 3, 10, 1, GETDATE(), null, null, null, null, 0),
(1, 4, 10, 1, GETDATE(), null, null, null, null, 0),
(1, 5, 10, 1, GETDATE(), null, null, null, null, 0),
(2, 1, 10, 1, GETDATE(), null, null, null, null, 0),
(2, 2, 10, 1, GETDATE(), null, null, null, null, 0),
(2, 3, 10, 1, GETDATE(), null, null, null, null, 0),
(2, 4, 10, 1, GETDATE(), null, null, null, null, 0),
(2, 5, 10, 1, GETDATE(), null, null, null, null, 0),
(3, 1, 10, 1, GETDATE(), null, null, null, null, 0),
(3, 2, 10, 1, GETDATE(), null, null, null, null, 0),
(3, 3, 10, 1, GETDATE(), null, null, null, null, 0),
(3, 4, 10, 1, GETDATE(), null, null, null, null, 0),
(3, 5, 10, 1, GETDATE(), null, null, null, null, 0)

select * from m_medical_facility_schedule
select * from t_doctor_office
--Table
Insert into m_medical_facility_schedule values
(1, 'Senin', '18.00', '21.00', 1, GETDATE(), null, null, null, null, 0),
(1, 'Selasa', '09.00', '12.00', 1, GETDATE(), null, null, null, null, 0),
(1, 'Rabu', '09.00', '12.00', 1, GETDATE(), null, null, null, null, 0),
(1, 'Kamis', '09.00', '12.00', 1, GETDATE(), null, null, null, null, 0),
(1, 'Jumat', '09.00', '12.00', 1, GETDATE(), null, null, null, null, 0),
(2, 'Senin', '08.00', '11.00', 1, GETDATE(), null, null, null, null, 0),
(2, 'Selasa', '09.00', '12.00', 1, GETDATE(), null, null, null, null, 0),
(2, 'Rabu', '10.00', '13.00', 1, GETDATE(), null, null, null, null, 0),
(2, 'Kamis', '11.00', '14.00', 1, GETDATE(), null, null, null, null, 0),
(2, 'Jumat', '12.00', '15.00', 1, GETDATE(), null, null, null, null, 0),
(3, 'Senin', '09.00', '12.00', 1, GETDATE(), null, null, null, null, 0),
(3, 'Selasa', '09.00', '12.00', 1, GETDATE(), null, null, null, null, 0),
(3, 'Rabu', '10.00', '13.00', 1, GETDATE(), null, null, null, null, 0),
(3, 'Kamis', '12.00', '15.00', 1, GETDATE(), null, null, null, null, 0),
(3, 'Jumat', '13.00', '16.00', 1, GETDATE(), null, null, null, null, 0)

-- Query Lokasi Praktek dan Jadwal Doctor
select bio.fullname as NamaDoctor, spe.name as NamaSpesialis, mdfc.name as NamaRS,
mdfs.day as Hari, mdfs.time_schedule_start as Mulai, mdfs.time_schedule_end as Selesai
from t_current_doctor_specialization as dsp
join m_specialization as spe on spe.Id = dsp.specialization_id
join m_doctor as doc on doc.Id = dsp.doctor_id
join m_biodata as bio on doc.biodata_id = bio.id
join t_doctor_office_schedule as drsc on doc.id = drsc.doctor_id
join m_medical_facility_schedule as mdfs on drsc.medical_facility_schedule_id = mdfs.id
join m_medical_facility as mdfc on mdfs.medical_facility_id = mdfc.id
join m_location as loc on mdfc.location_id = loc.id


-- Query Lokasi Praktek dan Jadwal Doctor dan harga jasa
select bio.fullname as NamaDoctor, medfac.name as NamaPoli, mdfc.name as NamaRS, 
mdfs.day as Hari, mdfs.time_schedule_start as Mulai, mdfs.time_schedule_end as Selesai,
dotp.price_start_from as HargaMulai
from t_current_doctor_specialization as dsp
join m_doctor as doc on doc.Id = dsp.doctor_id
join m_biodata as bio on doc.biodata_id = bio.id
join t_doctor_office_schedule as drsc on doc.id = drsc.doctor_id
join m_medical_facility_schedule as mdfs on drsc.medical_facility_schedule_id = mdfs.id
join m_medical_facility as mdfc on mdfs.medical_facility_id = mdfc.id
join m_medical_facility_category as medfac on medfac.id = mdfc.medical_facility_category_id
join t_doctor_office as dco on doc.id = dco.doctor_id
join t_doctor_office_treatment as dot on dot.doctor_office_id = dco.id
join t_doctor_office_treatment_price as dotp on dotp.doctor_office_treatment_id = dot.id

-- Select From
select * from t_current_doctor_specialization
select * from m_doctor
select * from m_biodata
select * from t_doctor_office_schedule
select * from m_medical_facility_schedule
select * from m_medical_facility
select * from m_medical_facility_category
select * from t_doctor_office -- belum diubah
select * from t_doctor_office_treatment
select * from t_doctor_office_treatment_price
select * from m_specialization

-- Truncate Table
truncate table t_doctor_office
truncate table m_doctor
truncate table m_doctor

-- Isi table
--Table Treatmen Doctor
select * from t_doctor_office -- belum diubah
Insert into t_doctor_office values
(1, 1, 'Dokter Anak', 1, GETDATE(), null, null, null, null, 0),
(1, 2,'Dokter Anak', 1, GETDATE(), null, null, null, null, 0),
(1, 3,'Dokter Anak', 1, GETDATE(), null, null, null, null, 0),
(2, 1, 'Dokter Gizi', 1, GETDATE(), null, null, null, null, 0),
(2, 2, 'Dokter Gizi', 1, GETDATE(), null, null, null, null, 0),
(2, 3,'Dokter Gizi', 1, GETDATE(), null, null, null, null, 0),
(3, 1, 'Dokter Kulit', 1, GETDATE(), null, null, null, null, 0),
(3, 2, 'Dokter Kulit', 1, GETDATE(), null, null, null, null, 0),
(3, 3,'Dokter Kulit', 1, GETDATE(), null, null, null, null, 0)

select * from m_medical_facility_category
select * from t_doctor_office

select * from t_doctor_office_treatment
truncate table t_doctor_office_treatment

insert into t_doctor_office_treatment values
(1, 1, 1, getdate(), null, null, null, null, 0),
(2, 1, 1, getdate(), null, null, null, null, 0),
(3, 1, 1, getdate(), null, null, null, null, 0),
(4, 1, 1, getdate(), null, null, null, null, 0),
(1, 2, 1, getdate(), null, null, null, null, 0),
(2, 2, 1, getdate(), null, null, null, null, 0),
(3, 2, 1, getdate(), null, null, null, null, 0),
(4, 2, 1, getdate(), null, null, null, null, 0),
(1, 3, 1, getdate(), null, null, null, null, 0),
(2, 3, 1, getdate(), null, null, null, null, 0),
(3, 3, 1, getdate(), null, null, null, null, 0),
(4, 3, 1, getdate(), null, null, null, null, 0)

select * from t_doctor_office_treatment_price -- Biaya treatmen
insert into t_doctor_office_treatment_price values
(1, 500000, 300000, 1000000, 1,  getdate(), null, null, null, null, 0),
(2, 600000, 400000, 1000000, 1,  getdate(), null, null, null, null, 0),
(3, 700000, 500000, 1000000, 1,  getdate(), null, null, null, null, 0),
(4, 800000, 600000, 1000000, 1,  getdate(), null, null, null, null, 0)



select * from t_doctor_office_schedule -- Schedule
select * from m_medical_facility_schedule -- Jadwal



select * from m_medical_facility_category -- Jenis Poli
select * from m_medical_facility_schedule -- Jadwal
select * from m_location -- Lokasi 
select * from m_specialization -- Spesilis 

select * from m_doctor
select * from m_doctor_education
select * from m_education_level

select * from m_doctor_education -- Riwayat Pendidikan
select * from t_doctor_treatment -- TIndakan Medis
select * from t_doctor_office_treatment
select * from t_doctor_office_schedule
select * from t_doctor_office_treatment_price
select * from t_doctor_office_schedule
select * from t_current_doctor_specialization
select * from t_appointment
select * from m_biodata_attachment
select * from m_biodata
select * from


