﻿using Med_Id.datamodels;
using Med_Id.viewmodels;
using Med_Id.web.Services;
using Microsoft.AspNetCore.Mvc;

namespace Med_Id.web.Controllers
{
	public class AuthController : Controller
	{
		private AuthService authService;
		VMResponse respon = new VMResponse();

		public AuthController(AuthService _authService)
		{
			this.authService = _authService;
		}

		public IActionResult Login()
		{
			return PartialView();
		}

		public IActionResult Logout()
		{
			HttpContext.Session.Clear();
			return RedirectToAction("Index", "Home");
		}


		[HttpPost]
		public async Task<JsonResult> LoginSubmit(string username)
		{
			//VMUser customer = await authService.CheckLogin(email, password);

			if ( (username == "Admin" || username == "Doctor" || username == "Pasien" || username == "Faskes" || username == "Umum"))
			{
				
				respon.Message = $"Hallo, {username} welcome to Med.ID";
				HttpContext.Session.SetString("NameUser", username);
				HttpContext.Session.SetInt32("IdUser", 1);
				if (username == "Admin")
				{
                    HttpContext.Session.SetInt32("IdRole", 1);
                }
                else if (username == "Doctor")
                {
                    HttpContext.Session.SetInt32("IdRole", 2);
                }
                else if (username == "Faskes")
                {
                    HttpContext.Session.SetInt32("IdRole", 3);
                }
                else if (username == "Pasien")
                {
                    HttpContext.Session.SetInt32("IdRole", 4);
                }
                else if (username == "Umum")
                {
                    HttpContext.Session.SetInt32("IdRole", 5);
                }
				else
				{
                    HttpContext.Session.SetInt32("IdRole", 9);
                }

                // username == "Admin" ? 1 : username == "Doctor" ? 2 : username == "Faskes" ? 3 : username == "Pasien" ? 4 : 6
            }
            else
			{
				respon.Message = $"Oops, {username} notfound or password is wrong, please check it!";
			}
			return Json(new { dataRespon = respon });
		}

		//[HttpPost]
		//public async Task<JsonResult> MenuAccess(long IdRole)
		//{
		//	VMUser customer = await authService.MenuAccess(IdRole);

		//	if (customer != null)
		//	{
		//		respon.Message = $"Hallo, {customer.Name} welcome to XPOS";
		//		HttpContext.Session.SetString("NameCustomer", customer.Name);
		//		HttpContext.Session.SetInt32("IdCustomer", customer.Id);
		//		HttpContext.Session.SetInt32("IdRole", customer.RoleId == 1);
		//	}
		//	else
		//	{
		//		respon.Message = "Oops, notfound or password is wrong, please check it!";
		//	}
		//	return Json(new { dataRespon = respon });
		//}
	}
}
