﻿using Med_Id.datamodels;
using Med_Id.viewmodels;
using Med_Id.web.Services;
using Microsoft.AspNetCore.Mvc;

namespace Med_Id.web.Controllers
{
    public class DoctorController : Controller
    {
        private BiodataService biodataService;
        private DoctorService doctorService;
        private int IdUser = 1;

        public DoctorController(DoctorService _doctorService, BiodataService _biodataService)
        {

            this.biodataService = _biodataService;
            this.doctorService = _doctorService;

        }
        public async Task<IActionResult> Index(string sortOrder,
                                                string searchString,
                                                string currentFilter,
                                                int? pageNumber,
                                                int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<VMDoctor> data = await doctorService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.Fullname.ToLower().Contains(searchString.ToLower())
                || a.Str.ToLower().Contains(searchString.ToLower())
                ).ToList();
            }
            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.Fullname).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.Fullname).ToList();
                    break;
            }
            return View(PaginationList<VMDoctor>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }
        public async Task<IActionResult> Create()
        {
            VMDoctor data = new VMDoctor();

            List<MBiodatum> listBiodata = await biodataService.GetAllData();
            ViewBag.ListBiodata = listBiodata;

            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Create(VMDoctor dataParam)
        {
            VMResponse respon = await doctorService.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }
        public async Task<IActionResult> Edit(int id)
        {
            VMDoctor data = await doctorService.GetDataById(id);

            List<MBiodatum> listBiodata = await biodataService.GetAllData();
            ViewBag.ListBiodata = listBiodata;

            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(VMDoctor dataParam)
        {
            VMResponse respon = await doctorService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }

        public async Task<IActionResult> Detail(int id)
        {
            VMDoctor data = await doctorService.GetDataById(id);



            List<MBiodatum> listBiodata = await biodataService.GetAllData();
            //ViewBag.ListBiodata = listBiodata;

            List<VMTindakanMedis> listTindakanMedis = await doctorService.GetDataByTindakanMedis(id);
            //ViewBag.ListTindakanMedis = listTindakanMedis;

            List<VMRiwayatLocation> listRiwayatLocation = await doctorService.GetDataByRiwayatLocation(id);
            //ViewBag.ListRiwayatLocation = listRiwayatLocation;

            List<VMLocationPraktek> listLocationPraktek = await doctorService.GetDataByLocationPraktek(id);
            //ViewBag.ListLocationPraktek = listLocationPraktek;

            List<VMEducationDoctor> listEducationDoctor = await doctorService.GetDataByEducationDoctor(id);

            //ViewBag.ListEducationDoctor = listEducationDoctor;
            VMNamaPoli datas = await doctorService.GetDataByNamaPoli(id);

            VMAllDoctor allData = new VMAllDoctor();

            allData.vmNamaPoli = datas;
            allData.vMDoctors = data;
            allData.tindakanMedis = listTindakanMedis;
            allData.locationPraktek = listLocationPraktek;
            allData.riwayatLocation = listRiwayatLocation;
            allData.educationDoctor = listEducationDoctor;


            return PartialView(allData);
        }
        [HttpPost]
        public async Task<IActionResult> Detail(VMDoctor dataParam)
        {
            VMResponse respon = await doctorService.Detail(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }

            return View(dataParam);
        }
        public async Task<IActionResult> Delete(int id)
        {
            VMDoctor data = await doctorService.GetDataById(id);
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> SureDelete(int id, int DeletedBy)
        {

            VMResponse respon = await doctorService.Delete(id, DeletedBy);

            if (respon.Success)
            {
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index", id);
        }

       
    }
}
