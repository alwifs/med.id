﻿using Med_Id.web.Services;
using Microsoft.AspNetCore.Mvc;
using Med_Id.datamodels;
using Med_Id.viewmodels;

namespace Med_Id.web.Controllers
{
    public class CourierController : Controller
    {
        private CourierService courierService;
        private int IdUser = 1;

        public CourierController(CourierService _courierService)
        {
            this.courierService = _courierService;

        }
        public async Task<IActionResult> Index(string sortOrder,
                                                string searchString,
                                                string currentFilter,
                                                int? pageNumber,
                                                int? pageSize)
        {
            ViewBag.CurrentSort = sortOrder;
            ViewBag.CurrentPageSize = pageSize;
            ViewBag.NameSort = string.IsNullOrEmpty(sortOrder) ? "name_desc" : "";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;

            List<MCourier> data = await courierService.GetAllData();

            if (!string.IsNullOrEmpty(searchString))
            {
                data = data.Where(a => a.Name.ToLower().Contains(searchString.ToLower())
                ).ToList();
            }
            switch (sortOrder)
            {
                case "name_desc":
                    data = data.OrderByDescending(a => a.Name).ToList();
                    break;
                default:
                    data = data.OrderBy(a => a.Name).ToList();
                    break;
            }
            return View(PaginationList<MCourier>.CreateAsync(data, pageNumber ?? 1, pageSize ?? 3));
        }

        public IActionResult Create()
        {
            MCourier data = new MCourier();
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Create(MCourier dataParam)
        {
            VMResponse respon = await courierService.Create(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }
       
        public async Task<IActionResult> Edit(int id)
        {
            MCourier data = await courierService.GetDataById(id);
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(MCourier dataParam)
        {
            VMResponse respon = await courierService.Edit(dataParam);

            if (respon.Success)
            {
                return Json(new { dataRespon = respon });
            }
            return View(dataParam);
        }
        public async Task<IActionResult> Delete(int id)
        {
            MCourier data = await courierService.GetDataById(id);
            return PartialView(data);
        }
        [HttpPost]
        public async Task<IActionResult> SureDelete(int id, int DeletedBy)
        {
            DeletedBy = IdUser;
            VMResponse respon = await courierService.Delete(id, DeletedBy);

            if (respon.Success)
            {
                return RedirectToAction("Index");
            }

            return RedirectToAction("Index", id);
        }
        public async Task<IActionResult> Detail(int id)
        {
            MCourier data = await courierService.GetDataById(id);
            return PartialView(data);
        }
        public async Task<JsonResult> CheckNameIsExist(string name)
        {
            bool isExist = await courierService.CheckCourierByName(name);
            return Json(isExist);
        }
    }
}
