﻿using Med_Id.viewmodels;
using Newtonsoft.Json;
using System.Text;

namespace Med_Id.web.Services
{
    public class DoctorService
    {
        private static readonly HttpClient client = new HttpClient();
        private IConfiguration configuration;
        private string RouteAPI = "";
        private VMResponse respon = new VMResponse();

        public DoctorService(IConfiguration _configuration)
        {
            this.configuration = _configuration;
            this.RouteAPI = this.configuration["RouteAPI"];
        }
        public async Task<List<VMDoctor>> GetAllData()
        {
            List<VMDoctor> data = new List<VMDoctor>();
            string apiResponse = await client.GetStringAsync(RouteAPI + "apiDoctor/GetAllData");

            data = JsonConvert.DeserializeObject<List<VMDoctor>>(apiResponse)!;

            return data;
        }
        public async Task<VMResponse> Create(VMDoctor dataParam)
        {
            // proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            /// proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            // proses memanggil api dan pengiriman body
            var request = await client.PostAsync(RouteAPI + "apiDoctor/Save", content);


            if (request.IsSuccessStatusCode)
            {
                // proses membaca respon api
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses 
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;

            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }
        public async Task<VMDoctor> GetDataById(int id)
        {
            VMDoctor data = new VMDoctor();
            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiDoctor/GetDataById/{id}");
            data = JsonConvert.DeserializeObject<VMDoctor>(apiRespon)!;

            return data;

        }
        public async Task<List<VMDoctor>> GetDataByDoctorId(int id)
        {
            List<VMDoctor> data = new List<VMDoctor>();
            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiDoctor/GetDataByDoctorId/{id}");
            data = JsonConvert.DeserializeObject<List<VMDoctor>>(apiRespon)!;

            return data;

        }

        public async Task<VMResponse> Edit(VMDoctor dataParam)
        {
            // proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            /// proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            // proses memanggil api dan pengiriman body
            var request = await client.PutAsync(RouteAPI + "apiDoctor/Edit", content);


            if (request.IsSuccessStatusCode)
            {
                // proses membaca respon api
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses 
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;

            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }
        public async Task<VMResponse> Detail(VMDoctor dataParam)
        {
            // proses convert dari object ke string
            string json = JsonConvert.SerializeObject(dataParam);
            /// proses mengubah string menjadi json lalu dikirim sebagai request body
            StringContent content = new StringContent(json, UnicodeEncoding.UTF8, "application/json");
            // proses memanggil api dan pengiriman body
            var request = await client.PutAsync(RouteAPI + "apiDoctor/Detail", content);


            if (request.IsSuccessStatusCode)
            {
                // proses membaca respon api
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses 
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;

            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }
            return respon;
        }
        public async Task<VMResponse> Delete(int id, int DeletedBy)
        {
            var request = await client.DeleteAsync(RouteAPI + $"apiDoctor/Delete/{id}/{DeletedBy}");

            if (request.IsSuccessStatusCode)
            {
                // proses membaca respon api
                var apiRespon = await request.Content.ReadAsStringAsync();

                // proses 
                respon = JsonConvert.DeserializeObject<VMResponse>(apiRespon)!;

            }
            else
            {
                respon.Success = false;
                respon.Message = $"{request.StatusCode} : {request.ReasonPhrase}";
            }

            return respon;
        }
        public async Task<List<VMTindakanMedis>> GetDataByTindakanMedis(int id)
        {
            List<VMTindakanMedis> data = new List<VMTindakanMedis>();
            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiDoctor/GetDataByTindakanMedis/{id}");
            data = JsonConvert.DeserializeObject<List<VMTindakanMedis>>(apiRespon)!;

            return data;

        }
        public async Task<List<VMEducationDoctor>> GetDataByEducationDoctor(int id)
        {
            List<VMEducationDoctor> data = new List<VMEducationDoctor>();
            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiDoctor/GetDataByEducationDoctor/{id}");
            data = JsonConvert.DeserializeObject<List<VMEducationDoctor>>(apiRespon)!;

            return data;

        }
        public async Task<List<VMRiwayatLocation>> GetDataByRiwayatLocation(int id)
        {
            List<VMRiwayatLocation> data = new List<VMRiwayatLocation>();
            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiDoctor/GetDataByRiwayatLocation/{id}");
            data = JsonConvert.DeserializeObject<List<VMRiwayatLocation>>(apiRespon)!;

            return data;

        }
        public async Task<List<VMLocationPraktek>> GetDataByLocationPraktek(int id)
        {
            List<VMLocationPraktek> data = new List<VMLocationPraktek>();
            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiDoctor/GetDataByLocationPraktek/{id}");
            data = JsonConvert.DeserializeObject<List<VMLocationPraktek>>(apiRespon)!;

            return data;

        }

        public async Task<VMNamaPoli> GetDataByNamaPoli(int id)
        {
            VMNamaPoli datas = new VMNamaPoli();
            string apiRespon = await client.GetStringAsync(RouteAPI + $"apiDoctor/GetDataByNamaPoli/{id}");
            datas = JsonConvert.DeserializeObject<VMNamaPoli>(apiRespon)!;

            return datas;

        }
    }
}
