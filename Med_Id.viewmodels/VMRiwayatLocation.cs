﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med_Id.viewmodels
{
    public class VMRiwayatLocation
    {
        public long Id { get; set; }
        public long? BiodataId { get; set; }
        public string? Fullname { get; set; }
        public long? DoctorId { get; set; }
        public long SpecializationId { get; set; }
        public string? NameSpesial { get; set; }
        public long? MedicalFacilityId { get; set; }
        public string? NameRs { get; set; }
        public string? NamaPoli { get; set; }
        public long? MedicalFacilityScheduleId { get; set; }
        public long? MedicalFacilityCategoryId { get; set; }
        public long? LocationId { get; set; }
        public string? NameLocation { get; set; }
        public DateTime? Mulai { get; set; }
        public DateTime? Selesai { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDelete { get; set; }

    }
}
