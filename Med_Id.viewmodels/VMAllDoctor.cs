﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med_Id.viewmodels
{
    public class VMAllDoctor
    {
        //public long Id { get; set; }
        //public long? DoctorId { get; set; }
        //public long? BiodataId { get; set; }
        //public string? Fullname { get; set; }
        //public long? EducationLevelId { get; set; }
        //public string? InstitutionName { get; set; }
        //public string? Major { get; set; }
        //public string? StartYear { get; set; }
        //public string? EndYear { get; set; }
        //public bool? IsLastEducation { get; set; }
        //public long CreatedBy { get; set; }

        //public long? MedicalFacilityId { get; set; }
        //public string? NameRs { get; set; }
        //public long? MedicalFacilityScheduleId { get; set; }
        //public long? MedicalFacilityCategoryId { get; set; }
        //public string? NamaPoli { get; set; }
        //public string? Hari { get; set; }
        //public string? Mulai { get; set; }
        //public string? Selesai { get; set; }
        //public string? HargaMulai { get; set; }

        //public long SpecializationId { get; set; }
        //public string? NameSpesialis { get; set; }

        //public long? LocationId { get; set; }
        //public string? NameLocation { get; set; }

        //public string? Str { get; set; }
        //public string? NameStr { get; set; }

        //public DateTime CreatedOn { get; set; }
        //public long? ModifiedBy { get; set; }
        //public DateTime? ModifiedOn { get; set; }
        //public long? DeletedBy { get; set; }
        //public DateTime? DeltedOn { get; set; }
        //public bool IsDelete { get; set; }





        public VMDoctor vMDoctors { get; set; } = null!;
        public List<VMTindakanMedis> tindakanMedis { get; set; } = null!;
        public List<VMEducationDoctor> educationDoctor { get; set; } = null!;
        public List<VMRiwayatLocation> riwayatLocation { get; set; } = null!;
        public List<VMLocationPraktek> locationPraktek { get; set; } = null!;

        public VMNamaPoli vmNamaPoli { get; set; } = null!;
    }
}
