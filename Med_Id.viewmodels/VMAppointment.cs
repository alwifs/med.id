﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Med_Id.viewmodels
{
    public class VMAppointment
    {
        public long Id { get; set; }
        public long? CustomerId { get; set; }
        public string? NameCostumer { get; set; }
        public long? DoctorOfficeId { get; set; }
        public string? NameDoctor { get; set; }
        public long? DoctorOfficeScheduleId { get; set; }
        public long? DoctorOfficeTreatmentId { get; set; }
        public string? NameTindakan { get; set; }
        public DateTime? AppointmentDate { get; set; }
        public string? NameRS { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedOn { get; set; }
        public long? DeletedBy { get; set; }
        public DateTime? DeletedOn { get; set; }
        public bool IsDelete { get; set; }
    }
}
