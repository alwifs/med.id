﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Med_Id.datamodels;
using Med_Id.viewmodels;

namespace Med_Id.Api.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class apiCourierController : ControllerBase
    {
        private readonly medidContext db;
        private VMResponse respon = new VMResponse();
        private int IdUser = 1;

        public apiCourierController(medidContext _db)
        {
            this.db = _db;
        }

        [HttpGet("GetAllData")]
        public List<MCourier> GetAllData()
        {
            List<MCourier> data = db.MCouriers.Where(a => a.IsDelete == false).ToList();
            return data;

        }
        [HttpGet("GetDataById/{id}")]
        public MCourier GetDataById(int id)
        {
            MCourier result = new MCourier();
            result = db.MCouriers.Where(a => a.Id == id).FirstOrDefault()!;
            return result;
        }

        [HttpPost("Save")]
        public VMResponse Save(MCourier data)
        {
            data.CreatedBy = IdUser;
            data.CreatedOn = DateTime.Now;
            data.IsDelete = false;

            try
            {
                db.Add(data); db.SaveChanges();

                respon.Message = "Data succses saved";
            }
            catch (Exception e)
            {
                respon.Success = false;
                respon.Message = "Failed Saved" + e.Message;
            }
            return respon;
        }

        [HttpPut("Edit")]
        public VMResponse Edit(MCourier data)
        {
            MCourier dta = db.MCouriers.Where(a => a.Id == data.Id).FirstOrDefault()!;

            if (dta != null)
            {

                dta.Name = data.Name;
         
                dta.ModifiedBy = IdUser;
                dta.ModifiedOn = DateTime.Now;
                
                dta.IsDelete = false;

                try
                {
                    db.Update(dta); db.SaveChanges();

                    respon.Message = "Data Succes Update";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "Data Failed" + e.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data Not Found";
            }
            return respon;
        }

        [HttpDelete("Delete/{id}/{DeletedBy}")]
        public VMResponse Delete(int id, int DeletedBy)
        {
            MCourier dta = db.MCouriers.Where(a => a.Id == id).FirstOrDefault()!;
                
            if (dta != null)
            {
                dta.IsDelete = true;
                
                dta.DeletedBy = IdUser;
                dta.DeletedOn = DateTime.Now;

                try
                {
                    db.Update(dta);
                    db.SaveChanges();

                    respon.Message = $"Data {dta.Name} success deleted";
                }
                catch (Exception e)
                {
                    respon.Success = false;
                    respon.Message = "Delete Failed" + e.Message;
                }

            }
            else
            {
                respon.Success = false;
                respon.Message = "Data not Found";
            }
            return respon;
        }
        [HttpGet("CheckCourierByName/{name}")]
        public bool CheckName(string name)
        {
            MCourier data = db.MCouriers.Where(a => a.Name == name && a.IsDelete != true).FirstOrDefault();
            if (data != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
