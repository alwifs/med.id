use [med.id]

-- Select All Table
select * from m_admin		
select * from m_bank
select * from m_biodata		
select * from m_biodata_address		
select * from m_blood_group		
select * from m_courier		
select * from m_courier_type		
select * from t_courier_discount		
select * from m_customer		
select * from m_customer_member		
select * from m_customer_relation		
select * from m_doctor		
select * from m_doctor_education		
select * from m_education_level		
select * from m_location		
select * from m_location_level		
select * from m_medical_facility		
select * from m_medical_facility_category		
select * from m_medical_facility_schedule		
select * from m_medical_item		
select * from m_medical_item_category		
select * from m_medical_item_segmentation		
select * from m_menu		
select * from m_menu_role		
select * from m_payment_method		
select * from m_role		
select * from m_specialization		
select * from m_user		
select * from m_biodata_attachment		
select * from m_wallet_default_nominal		
select * from t_appointment		
select * from t_appointment_cancellation		
select * from t_appointment_done		
select * from t_appointment_reschedule_history		
select * from t_current_doctor_specialization		
select * from t_customer_chat		
select * from t_customer_chat_history		
select * from t_customer_custom_nominal		
select * from t_customer_registered_card		
select * from t_customer_va		
select * from t_customer_va_history		
select * from t_customer_wallet		
select * from t_customer_wallet_top_up		
select * from t_doctor_office		
select * from t_doctor_office_schedule		
select * from t_doctor_office_treatment		
select * from t_doctor_office_treatment_price		
select * from t_doctor_treatment		
select * from t_medical_item_purchase		
select * from t_medical_item_purchase_detail		
select * from t_reset_password		
select * from t_token
select * from t_treatment_discount		
select * from t_customer_wallet_withdraw

-- Riwayat kedatangan
select * from m_customer
select * from m_biodata
select * from m_blood_group
select * from m_doctor
select * from m_medical_item
select * from m_medical_item_category
select * from m_medical_item_segmentation
select * from m_location
select * from m_specialization
select *  from t_current_doctor_specialization
select * from t_doctor_office_treatment
select * from t_doctor_office
select * from t_doctor_office_schedule
select * from t_doctor_treatment
select * from t_appointment
select * from t_appointment_cancellation		
select * from t_appointment_done		
select * from 

-- Tipe Golongan Darah
select * from m_blood_group
truncate table m_blood_group
insert into m_blood_group values
-- Tambah Biodata Pasien
('A+', 'Golongan Darah Tipe A+', 1, getdate(), null, null, null, null, 0),
('A-', 'Golongan Darah Tipe A-', 1, getdate(), null, null, null, null, 0),
('B+', 'Golongan Darah Tipe B+', 1, getdate(), null, null, null, null, 0),
('B-', 'Golongan Darah Tipe B-', 1, getdate(), null, null, null, null, 0),
('O+', 'Golongan Darah Tipe O+', 1, getdate(), null, null, null, null, 0),
('O-', 'Golongan Darah Tipe O-', 1, getdate(), null, null, null, null, 0),
('AB+', 'Golongan Darah Tipe AB+', 1, getdate(), null, null, null, null, 0),
('AB-', 'Golongan Darah Tipe AB-', 1, getdate(), null, null, null, null, 0)

-- Tambah Pasien/ Biodata
select * from m_biodata
select * from m_blood_group
select * from m_customer
truncate table m_customer
insert into m_customer values
( 5, '1997/01/01', 'L', 1, '(+)', 175, 80, 1, getdate(), null, null, null, null, 0),
( 6, '1996/03/31', 'L', 7, '(+)', 175, 100, 1, getdate(), null, null, null, null, 0),
( 7, '1998/08/05', 'L', 5, '(+)', 175, 65, 1, getdate(), null, null, null, null, 0)


-- Tambah Dokter/ Biodata
select * from m_biodata
truncate table m_biodata
insert into m_biodata values
-- Tambah Biodata Pasien
('Elga', '081234567890', null,null, 1, getdate(), null, null, null, null,0),
('Alwi', '081234567890', null,null, 1, getdate(), null, null, null, null,0),
('Toni', '081234567890', null,null, 1, getdate(), null, null, null, null,0)

-- Tambah Biodata Dokter
('drg. Kania', '081234567890', null,null, 1, getdate(), null, null, null, null,0)


truncate table m_doctor
select * from m_doctor
insert into m_doctor values	
(4, 'DR004', 1, getdate(), null, null, null, null,0)

-- Isi data Tindakan Medis
--Table Treatmen Doctor
select * from t_doctor_treatment
Insert into t_doctor_treatment values
(4, 'Cabut Gigi',1, GETDATE(), null, null, null, null, 0),
(4, 'Pasang Gigi',1, GETDATE(), null, null, null, null, 0),
(4, 'Pasang Behel Gigi',1, GETDATE(), null, null, null, null, 0)

--Quwery Doctor
select bio.fullname as NamaDoctor
from m_doctor as doc
join m_biodata as bio on doc.biodata_id = bio.id

--Quwery Costumer
select bio.fullname as NamaCostumer, cust.dob as TanggalLahir, cust.gender as JenisKelamin, 
blood.code as GolDarah, cust.rhesus_type as RhesusType, cust.weight as BeratBadan, cust.height as TinggiBadan
from m_customer as cust
join m_biodata as bio on cust.biodata_id = bio.id
join m_blood_group as blood on cust.blood_group_id = blood.id
 
--Quwery Tindakan Medis Doctor
select bio.fullname as NamaDoctor, tdm.name as TindakanMedis
from t_doctor_treatment as tdm
join m_doctor as doc on doc.id = tdm.doctor_id
join m_biodata as bio on doc.biodata_id = bio.id

-- Query Riwayat Kedatangan
select * from m_customer
select * from m_biodata
select * from m_blood_group
select * from m_doctor
select * from m_medical_item
select * from m_medical_item_category
select * from m_medical_item_segmentation
select * from m_location
select * from m_specialization
select * from t_current_doctor_specialization
select * from t_doctor_office_treatment
select * from t_doctor_office
select * from t_doctor_office_schedule
select * from t_doctor_treatment
select * from t_appointment
select * from t_appointment_cancellation		
select * from t_appointment_done		
select * from m_medical_facility

select
bio.fullname as NamaCostumer, appoint.appointment_date as TanggalMasuk, doctreat.name as TindakanMedis,
medfac.name as NamaRs
from m_customer as cust
join m_biodata as bio on cust.biodata_id = bio.id
join m_blood_group as blood on cust.blood_group_id = blood.id
join t_appointment as appoint on cust.id = appoint.customer_id
join t_appointment_done as appdone on appoint.Id = appdone.appointment_id
join t_doctor_office as docoff on appoint.doctor_office_id = docoff.id
join m_doctor as doc on docoff.doctor_id = doc.id
join m_medical_facility as medfac on docoff.medical_facility_id = medfac.id
join t_doctor_treatment as doctreat on doc.id = doctreat.doctor_id


select *
from m_customer as cust
join m_biodata as bio on cust.biodata_id = bio.id
join m_user as usr on bio.id = usr.biodata_id
join m_blood_group as blood on cust.blood_group_id = blood.id
join t_appointment as appoint on cust.id = appoint.customer_id
join t_appointment_done as appdone on appoint.Id = appdone.appointment_id
join t_doctor_office as docoff on appoint.doctor_office_id = docoff.id
join m_doctor as doc on docoff.doctor_id = doc.id
join m_medical_facility as medfac on docoff.medical_facility_id = medfac.id
join t_doctor_treatment as doctreat on doc.id = doctreat.doctor_id

select * from m_role
select * from m_customer
select * from m_biodata
select * from m_user
select * from m_blood_group
select * from t_appointment
select * from t_appointment_done
select * from m_doctor
select * from t_doctor_treatment
select * from t_doctor_office
select * from m_medical_facility


-- Mengisi data appointment
Insert into t_appointment values
(1, 10, 9, 4, GETDATE(), 1, GETDATE(), null, null, null, null, 0)

select * from t_appointment_done
Insert into t_appointment_done values
(1, 'Berdasakan hasil pemeriksaan, gigi geraham terakhir harus dilakukan pencabutan atau biasa disebut operasi atau pembedahan', 1, GETDATE(), null, null, null, null, 0)


-- Isi table
--Table Treatment Doctor
select * from t_doctor_office
Insert into t_doctor_office values
(4, 1, 'Dokter Gigi', '2019/01/01', null, 1, GETDATE(), null, null, null, null, 0),
(4, 2,'Dokter Gigi', '2020/01/01', null, 1, GETDATE(), null, null, null, null, 0),
(4, 3,'Dokter Gigi', '2021/01/01', null, 1, GETDATE(), null, null, null, null, 0)

select * from t_doctor_treatment
select * from t_doctor_office_treatment
insert into t_doctor_office_treatment values
(16, 10, 1, getdate(), null, null, null, null, 0)

select * from t_doctor_office_schedule
Insert into t_doctor_office_schedule values
(4, 1, 10, 1, GETDATE(), null, null, null, null, 0)
