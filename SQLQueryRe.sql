use [med.id]
-- Query Riwayat Praktek Doctor
select bio.fullname as NamaDoctor, spe.name as NamaSpesialis, mdfc.name as NamaRS, loc.name as LocName
from t_current_doctor_specialization as dsp
join m_specialization as spe on spe.Id = dsp.specialization_id
join m_doctor as doc on doc.Id = dsp.doctor_id
join m_biodata as bio on doc.biodata_id = bio.id
join t_doctor_office_schedule as drsc on doc.id = drsc.doctor_id
join m_medical_facility_schedule as mdfs on drsc.medical_facility_schedule_id = mdfs.id
join m_medical_facility as mdfc on mdfs.medical_facility_id = mdfc.id
join m_location as loc on mdfc.location_id = loc.id

select *  from t_current_doctor_specialization
select *  from m_specialization
select *  from m_doctor
select *  from m_biodata
select *  from t_doctor_office_schedule
select *  from m_medical_facility_schedule
select *  from m_medical_facility
select *  from m_medical_facility_category
select *  from m_location
select *  from m_location_level

-- Query Lokasi Praktek dan Jadwal Doctor dan harga jasa
select distinct bio.fullname as NamaDoctor, medfac.name as NamaPoli, mdfc.full_address as Alamat, mdfc.name as NamaRS, 
mdfs.day as Hari, mdfs.time_schedule_start as Mulai, mdfs.time_schedule_end as Selesai,
dotp.price_start_from as HargaMulai
from t_current_doctor_specialization as dsp
join m_doctor as doc on doc.Id = dsp.doctor_id
join m_biodata as bio on doc.biodata_id = bio.id
join t_doctor_office_schedule as drsc on doc.id = drsc.doctor_id
join m_medical_facility_schedule as mdfs on drsc.medical_facility_schedule_id = mdfs.id
join m_medical_facility as mdfc on mdfs.medical_facility_id = mdfc.id
join m_medical_facility_category as medfac on medfac.id = mdfc.medical_facility_category_id
join t_doctor_office as dco on doc.id = dco.doctor_id
join t_doctor_office_treatment as dot on dot.doctor_office_id = dco.id
join t_doctor_office_treatment_price as dotp on dotp.doctor_office_treatment_id = dot.id
	
select * from t_current_doctor_specialization
select * from m_specialization
select * from m_doctor
select * from m_biodata
select * from t_doctor_office_schedule
select * from m_medical_facility_schedule
select * from m_medical_facility
select * from m_medical_facility_category
select * from t_doctor_office -- belum diubah
select * from t_doctor_office_treatment
select * from t_doctor_office_treatment_price
select * from m_specialization
select * from t_doctor_treatment


select *  from m_location
select *  from m_location_level

truncate table m_medical_facility_schedule
truncate table t_doctor_office_schedule

select *  from m_medical_facility_schedule
select *  from m_medical_facility
-- Insert Table
Insert into m_medical_facility_schedule values
(1, 'Kamis', '12.00', '15.00', 1, GETDATE(), null, null, null, null, 0)


(1, 'Kamis', '18.00', '21.00', 1, GETDATE(), null, null, null, null, 0),
(2, 'Jumat', '09.00', '12.00', 1, GETDATE(), null, null, null, null, 0),
(3, 'Rabu', '12.00', '15.00', 1, GETDATE(), null, null, null, null, 0)

select *  from t_doctor_office_schedule
select *  from m_medical_facility_schedule
select *  from m_medical_facility

select *  from m_doctor
select *  from m_biodata
-- Insert Table
Insert into t_doctor_office_schedule values
(1, 5, 10, 1, GETDATE(), null, null, null, null, 0)

(1, 1, 10, 1, GETDATE(), null, null, null, null, 0),
(1, 2, 10, 1, GETDATE(), null, null, null, null, 0),
(1, 3, 10, 1, GETDATE(), null, null, null, null, 0),
(2, 1, 10, 1, GETDATE(), null, null, null, null, 0),
(2, 2, 10, 1, GETDATE(), null, null, null, null, 0),
(2, 3, 10, 1, GETDATE(), null, null, null, null, 0),
(3, 1, 10, 1, GETDATE(), null, null, null, null, 0),
(3, 2, 10, 1, GETDATE(), null, null, null, null, 0),
(3, 3, 10, 1, GETDATE(), null, null, null, null, 0)

truncate table t_doctor_office
select * from t_doctor_office -- belum diubah
select * from m_medical_facility -- belum diubah

Insert into t_doctor_office values
(1, 1, 'Dokter Anak', '2019/01/01', null, 1, GETDATE(), null, null, null, null, 0),
(1, 2,'Dokter Anak', '2018/01/01', null, 1, GETDATE(), null, null, null, null, 0),
(1, 3,'Dokter Anak', '2016/01/01', '2018/12/30', 1, GETDATE(), null, null, null, null, 0),
(2, 1, 'Dokter Gizi', '2018/01/01', null, 1, GETDATE(), null, null, null, null, 0),
(2, 2, 'Dokter Gizi', '2020/01/01', null, 1, GETDATE(), null, null, null, null, 0),
(2, 3,'Dokter Gizi', '2021/01/01', null, 1, GETDATE(), null, null, null, null, 0),
(3, 1, 'Dokter Kulit', '2017/01/01', null, 1, GETDATE(), null, null, null, null, 0),
(3, 2, 'Dokter Kulit', '2016/01/01','2018/12/30', 1, GETDATE(), null, null, null, null, 0),
(3, 3,'Dokter Kulit', '2014/01/01', null, 1, GETDATE(), null, null, null, null, 0)
	
drop table t_doctor_office

CREATE TABLE [dbo].[t_doctor_office](
	[id] [bigint] primary key IDENTITY(1,1) NOT NULL,
	[doctor_id] [bigint] NULL,
	[medical_facility_id] [bigint] NULL,
	[specialization] [varchar](100) NULL,
	[start_date] [datetime] NULL,
	[end_date] [datetime] NULL,
	[create_by] [bigint] NOT NULL,
	[create_on] [datetime] NOT NULL,
	[modified_by] [bigint] NULL,
	[modified_on] [datetime] NULL,
	[delete_by] [bigint] NULL,
	[delete_on] [datetime] NULL,
	[is_delete] [bit] NOT NULL,
	)



select * from t_doctor_office_treatment
select * from t_doctor_office
select * from m_medical_facility
select * from m_medical_facility_category
select * from m_medical_facility_schedule
select * from t_doctor_treatment
select * from t_doctor_office_treatment
select * from t_doctor_office_treatment_price

truncate table t_doctor_office_treatment
select * from t_doctor_office_treatment
select * from t_doctor_treatment

select * from t_doctor_office_treatment
select * from t_doctor_office
insert into t_doctor_office_treatment values
(1, 1, 1,  getdate(), null, null, null, null, 0),
(2, 2, 1,  getdate(), null, null, null, null, 0),
(3, 3, 1,  getdate(), null, null, null, null, 0),
(4, 1, 1,  getdate(), null, null, null, null, 0),
(5, 2, 1,  getdate(), null, null, null, null, 0),
(6, 3, 1,  getdate(), null, null, null, null, 0),
(7, 1, 1,  getdate(), null, null, null, null, 0),
(8, 4, 1,  getdate(), null, null, null, null, 0),
(9, 5, 1,  getdate(), null, null, null, null, 0),
(10, 6, 1,  getdate(), null, null, null, null, 0),
(11, 4, 1,  getdate(), null, null, null, null, 0),
(12, 5, 1,  getdate(), null, null, null, null, 0),
(13, 7, 1,  getdate(), null, null, null, null, 0),
(14, 8, 1,  getdate(), null, null, null, null, 0),
(15, 9, 1,  getdate(), null, null, null, null, 0)


-- Query Lokasi Praktek dan Jadwal Doctor dan harga jasa
select bio.fullname as NamaDoctor, medfac.name as NamaPoli, mdfc.name as NamaRS, 
mdfs.day as Hari, mdfs.time_schedule_start as Mulai, mdfs.time_schedule_end as Selesai,
dotp.price_start_from as HargaMulai
from t_current_doctor_specialization as dsp
join m_doctor as doc on doc.Id = dsp.doctor_id
join m_biodata as bio on doc.biodata_id = bio.id
join t_doctor_office_schedule as drsc on doc.id = drsc.doctor_id
join m_medical_facility_schedule as mdfs on drsc.medical_facility_schedule_id = mdfs.id
join m_medical_facility as mdfc on mdfs.medical_facility_id = mdfc.id
--
join m_medical_facility_category as medfac on medfac.id = mdfc.medical_facility_category_id
join t_doctor_office as dco on doc.id = dco.doctor_id
join t_doctor_office_treatment as dot on dot.doctor_office_id = dco.id
join t_doctor_office_treatment_price as dotp on dotp.doctor_office_treatment_id = dot.id

select * from t_current_doctor_specialization
select * from m_specialization
select * from m_doctor
select * from m_biodata
select * from t_doctor_office_schedule
select * from m_medical_facility_schedule
select * from m_medical_facility
--
select * from m_medical_facility_category
select * from t_doctor_treatment
select * from t_doctor_office -- belum diubah
select * from t_doctor_office_treatment
select * from t_doctor_office_treatment_price
select * from m_specialization

select * from t_doctor_office_treatment_price
truncate table t_doctor_office_treatment_price
insert into t_doctor_office_treatment_price values
(2, 30000, 300000, 1000000, 1,  getdate(), null, null, null, null, 0),
(3, 30000, 300000, 1000000, 1,  getdate(), null, null, null, null, 0),
(4, 30000, 300000, 1000000, 1,  getdate(), null, null, null, null, 0),
(5, 30000, 300000, 1000000, 1,  getdate(), null, null, null, null, 0),
(6, 30000, 300000, 1000000, 1,  getdate(), null, null, null, null, 0),
(7, 30000, 300000, 1000000, 1,  getdate(), null, null, null, null, 0),
(9, 30000, 300000, 1000000, 1,  getdate(), null, null, null, null, 0),
(10, 30000, 300000, 1000000, 1,  getdate(), null, null, null, null, 0),
(11, 30000, 300000, 1000000, 1,  getdate(), null, null, null, null, 0),
(12, 30000, 300000, 1000000, 1,  getdate(), null, null, null, null, 0),
(13, 30000, 300000, 1000000, 1,  getdate(), null, null, null, null, 0),
(14, 30000, 300000, 1000000, 1,  getdate(), null, null, null, null, 0),
(15, 30000, 300000, 1000000, 1,  getdate(), null, null, null, null, 0)


-- Query Riwayat Praktek Doctor
select bio.fullname as NamaDoctor, spe.name as NamaSpesialis, mdfc.name as NamaRS, loc.name as LocName, dco.start_date as Mulai,
dco.end_date as Akhir
from t_current_doctor_specialization as dsp
join m_specialization as spe on spe.Id = dsp.specialization_id
join m_doctor as doc on doc.Id = dsp.doctor_id
join m_biodata as bio on doc.biodata_id = bio.id
join t_doctor_office_schedule as drsc on doc.id = drsc.doctor_id
join m_medical_facility_schedule as mdfs on drsc.medical_facility_schedule_id = mdfs.id
join m_medical_facility as mdfc on mdfs.medical_facility_id = mdfc.id
join m_location as loc on mdfc.location_id = loc.id
join t_doctor_office as dco on doc.Id = dco.doctor_id

select *  from t_current_doctor_specialization
select *  from m_specialization
select *  from m_doctor
select *  from m_biodata
select *  from t_doctor_office_schedule
select *  from m_medical_facility_schedule
select *  from m_medical_facility
select *  from m_medical_facility_category
select *  from m_location
select *  from m_location_level
select * from t_doctor_office -- belum diubah


insert into m_menu values
('Faskes 3', 'Index', 1, 'fa-solid fa-tree', 'far fa-star' ,1, getdate(), null, null, null, null, 0)

select * from m_menu_role
insert into m_menu_role values
(58,3, 1, getdate(), null, null, null, null, 0) -- Menu Profil/ Biodata

delete from  m_menu where id = 57
delete from  m_menu_role where id = 78

select * from m_menu
select * from m_role

select * from m_courier
delete from  m_courier where id = 57


-- New Query

use [med.id]

select * from m_biodata
select * from m_courier
select * from m_courier_type
select * from m_doctor
select * from m_doctor_education
select * from m_education_level
select * from m_location
select * from m_location_level
select * from m_medical_facility
select * from  m_medical_facility_category
select * from m_medical_facility_schedule

select * from t_doctor_office
select * from t_doctor_office_schedule
select * from t_doctor_office_treatment
select * from t_doctor_office_treatment_price
select * from t_doctor_treatment

select * from t_doctor_office_schedule
select * from m_doctor
select * from m_biodata
select * from m_medical_facility_schedule
select * from m_medical_facility
select * from m_medical_facility_category

-- Query Lokasi Praktek dan Jadwal Doctor dan harga jasa
select distinct bio.fullname as NamaDoctor, medfac.name as NamaPoli, mdfc.full_address as Alamat, mdfc.name as NamaRS, 
mdfs.day as Hari, mdfs.time_schedule_start as Mulai, mdfs.time_schedule_end as Selesai,
dotp.price_start_from as HargaMulai
from t_current_doctor_specialization as dsp
join m_doctor as doc on doc.Id = dsp.doctor_id
join m_biodata as bio on doc.biodata_id = bio.id
join t_doctor_office_schedule as drsc on doc.id = drsc.doctor_id
join m_medical_facility_schedule as mdfs on drsc.medical_facility_schedule_id = mdfs.id
join m_medical_facility as mdfc on mdfs.medical_facility_id = mdfc.id
join m_medical_facility_category as medfac on medfac.id = mdfc.medical_facility_category_id
join t_doctor_office as dco on doc.id = dco.doctor_id
join t_doctor_office_treatment as dot on dot.doctor_office_id = dco.id
join t_doctor_office_treatment_price as dotp on dotp.doctor_office_treatment_id = dot.id
	

-- Query Location Praktek Baru
select bio.fullname as NamaDokter, medfac.name as NamaRS, medfac.full_address as Alamat,
medfacat.name as NamaPoli, medsche.day as NamaJadwal, medsche.time_schedule_start as Mulai, medsche.time_schedule_end as Selesai
from t_doctor_office_schedule as docsche
join m_doctor as doc on docsche.doctor_id = doc.id
join m_biodata as bio on doc.biodata_id = bio.id
join m_medical_facility_schedule as medsche on docsche.medical_facility_schedule_id = medsche.id
join m_medical_facility as medfac on medsche.medical_facility_id = medfac.id
join m_medical_facility_category as medfacat on medfac.medical_facility_category_id = medfac.id


select * from m_medical_facility_schedule
select * from t_doctor_office_schedule
truncate table t_doctor_office_schedule
Insert into t_doctor_office_schedule values
(2, 1, 10, 1, GETDATE(), null, null, null, null, 0)

(1, 1, 10, 1, GETDATE(), null, null, null, null, 0),
(1, 2, 10, 1, GETDATE(), null, null, null, null, 0),
(1, 3, 10, 1, GETDATE(), null, null, null, null, 0),
(2, 4, 10, 1, GETDATE(), null, null, null, null, 0),
(2, 5, 10, 1, GETDATE(), null, null, null, null, 0),
(3, 6, 10, 1, GETDATE(), null, null, null, null, 0),
(3, 7, 10, 1, GETDATE(), null, null, null, null, 0)

(3, 2, 10, 1, GETDATE(), null, null, null, null, 0),
(3, 3, 10, 1, GETDATE(), null, null, null, null, 0)

truncate table m_medical_facility_category
select * from m_medical_facility_category
Insert into m_medical_facility_category values
('Poliklinik Anak', 1, GETDATE(), null, null, null, null, 0)



truncate table t_doctor_office
select * from t_doctor_office
Insert into t_doctor_office values
(1, 1, 'Dokter Anak', '2019/01/01', null, 1, GETDATE(), null, null, null, null, 1),
(1, 2,'Dokter Anak', '2018/01/01', null, 1, GETDATE(), null, null, null, null, 1),
(1, 3,'Dokter Anak', '2016/01/01', '2018/12/30', 1, GETDATE(), null, null, null, null, 1),
(2, 1, 'Dokter Gizi', '2018/01/01', null, 1, GETDATE(), null, null, null, null, 1),
(2, 2, 'Dokter Gizi', '2020/01/01', null, 1, GETDATE(), null, null, null, null, 1),
(2, 3,'Dokter Gizi', '2021/01/01', null, 1, GETDATE(), null, null, null, null, 1),
(3, 1, 'Dokter Kulit', '2017/01/01', null, 1, GETDATE(), null, null, null, null, 1),
(3, 2, 'Dokter Kulit', '2016/01/01','2018/12/30', 1, GETDATE(), null, null, null, null, 1),
(3, 3,'Dokter Kulit', '2014/01/01', null, 1, GETDATE(), null, null, null, null, 1),
(1, 3,'Dokter Anak', '2020/01/01', null, 1, GETDATE(), null, null, null, null, 0)


--Riwayat Praktek
select bio.fullname as Nama, mdfc.name as Nama_RS, mdloc.name as NamaLokasi, 
dcof.start_date as Mulai, dcof.end_date as Akhir, dcof.specialization as NamaSpesial
from t_doctor_office as dcof
join m_doctor as doc on doc.id = dcof.doctor_id
join m_biodata as bio on doc.biodata_id = bio.id
join m_medical_facility as mdfc on dcof.medical_facility_id = mdfc.id
join m_medical_facility_category as mdfccat on mdfc.medical_facility_category_id = mdfccat.id
join m_location as mdloc on mdfc.location_id = mdloc.id

--Location Praktek
select * from m_medical_facility
select * from m_medical_facility_schedule
truncate table m_medical_facility_schedule
--Table
Insert into m_medical_facility_schedule values
(1, 'Senin', '18.00', '21.00', 1, GETDATE(), null, null, null, null, 0),
(1, 'Selasa', '09.00', '12.00', 1, GETDATE(), null, null, null, null, 0),
(2, 'Rabu', '09.00', '12.00', 1, GETDATE(), null, null, null, null, 0),
--(2, 'Kamis', '09.00', '12.00', 1, GETDATE(), null, null, null, null, 0),
--(2, 'Jumat', '09.00', '12.00', 1, GETDATE(), null, null, null, null, 0),
--(3, 'Sabtu', '09.00', '12.00', 1, GETDATE(), null, null, null, null, 0),
--(3, 'Minggu', '09.00', '12.00', 1, GETDATE(), null, null, null, null, 0),

--(1, 'Senin', '08.00', '11.00', 1, GETDATE(), null, null, null, null, 0),
--(1, 'Selasa', '09.00', '12.00', 1, GETDATE(), null, null, null, null, 0),
--(2, 'Rabu', '10.00', '13.00', 1, GETDATE(), null, null, null, null, 0),
(2, 'Kamis', '09.00', '12.00', 1, GETDATE(), null, null, null, null, 0),
(3, 'Jumat', '14.00', '17.00', 1, GETDATE(), null, null, null, null, 0),
--(3, 'Sabtu', '11.00', '14.00', 1, GETDATE(), null, null, null, null, 0),
--(2, 'Minggu', '09.00', '12.00', 1, GETDATE(), null, null, null, null, 0),

--(1, 'Senin', '08.00', '11.00', 1, GETDATE(), null, null, null, null, 0),
--(1, 'Selasa', '09.00', '12.00', 1, GETDATE(), null, null, null, null, 0),
--(2, 'Rabu', '09.00', '12.00', 1, GETDATE(), null, null, null, null, 0),
--(2, 'Kamis', '09.00', '12.00', 1, GETDATE(), null, null, null, null, 0),
--(3, 'Jumat', '09.00', '12.00', 1, GETDATE(), null, null, null, null, 0),
(3, 'Sabtu', '09.00', '12.00', 1, GETDATE(), null, null, null, null, 0),
(3, 'Minggu', '09.00', '12.00', 1, GETDATE(), null, null, null, null, 0)

truncate table t_doctor_office
select * from m_medical_facility_schedule
select * from t_doctor_office
Insert into t_doctor_office values
(1, 1, 'Dokter Anak', '2019/01/01', null, 1, GETDATE(), null, null, null, null, 0),
(1, 2,'Dokter Anak', '2018/01/01', null, 1, GETDATE(), null, null, null, null, 0),
(1, 3,'Dokter Anak', '2016/01/01', '2018/12/30', 1, GETDATE(), null, null, null, null, 1),
(2, 1, 'Dokter Gizi', '2018/01/01', null, 1, GETDATE(), null, null, null, null, 0),
(2, 2, 'Dokter Gizi', '2020/01/01', null, 1, GETDATE(), null, null, null, null, 0),
(2, 3,'Dokter Gizi', '2021/01/01', null, 1, GETDATE(), null, null, null, null, 0),
(3, 1, 'Dokter Kulit', '2017/01/01', null, 1, GETDATE(), null, null, null, null, 0),
(3, 2, 'Dokter Kulit', '2016/01/01','2018/12/30', 1, GETDATE(), null, null, null, null, 1),
(3, 3,'Dokter Kulit', '2014/01/01', null, 1, GETDATE(), null, null, null, null, 0)

truncate table t_doctor_office_schedule
select * from t_doctor_office_schedule
select * from m_medical_facility_schedule
Insert into t_doctor_office_schedule values
(1, 1, 10, 1, GETDATE(), null, null, null, null, 0),
(1, 2, 10, 1, GETDATE(), null, null, null, null, 0),
(1, 3, 10, 1, GETDATE(), null, null, null, null, 0),
(1, 4, 10, 1, GETDATE(), null, null, null, null, 0),
(1, 5, 10, 1, GETDATE(), null, null, null, null, 0),
(1, 6, 10, 1, GETDATE(), null, null, null, null, 0),
(1, 7, 10, 1, GETDATE(), null, null, null, null, 0),

(2, 8, 10, 1, GETDATE(), null, null, null, null, 0),
(2, 9, 10, 1, GETDATE(), null, null, null, null, 0),
(2, 10, 10, 1, GETDATE(), null, null, null, null, 0),
(2, 11, 10, 1, GETDATE(), null, null, null, null, 0),
(2, 12, 10, 1, GETDATE(), null, null, null, null, 0),
(2, 13, 10, 1, GETDATE(), null, null, null, null, 0),
(2, 14, 10, 1, GETDATE(), null, null, null, null, 0),

(3, 15, 10, 1, GETDATE(), null, null, null, null, 0),
(3, 16, 10, 1, GETDATE(), null, null, null, null, 0),
(3, 17, 10, 1, GETDATE(), null, null, null, null, 0),
(3, 18, 10, 1, GETDATE(), null, null, null, null, 0),
(3, 19, 10, 1, GETDATE(), null, null, null, null, 0),
(3, 20, 10, 1, GETDATE(), null, null, null, null, 0),
(3, 21, 10, 1, GETDATE(), null, null, null, null, 0)


select * from t_doctor_office_treatment
select * from t_doctor_office_treatment_price
select * from t_doctor_treatment
select * from t_doctor_office
select * from m_medical_facility

-- Query Lokasi Praktek dan Jadwal Doctor dan harga jasa
select distinct bio.fullname as NamaDoctor, medfac.name as NamaPoli, mdfc.full_address as Alamat, mdfc.name as NamaRS, 
mdfs.day as Hari, mdfs.time_schedule_start as Mulai, mdfs.time_schedule_end as Selesai,
dotp.price_start_from as HargaMulai
from t_current_doctor_specialization as dsp
join m_doctor as doc on doc.Id = dsp.doctor_id
join m_biodata as bio on doc.biodata_id = bio.id
join t_doctor_office_schedule as drsc on doc.id = drsc.doctor_id
join m_medical_facility_schedule as mdfs on drsc.medical_facility_schedule_id = mdfs.id
join m_medical_facility as mdfc on mdfs.medical_facility_id = mdfc.id
join m_medical_facility_category as medfac on medfac.id = mdfc.medical_facility_category_id
join t_doctor_office as dco on doc.id = dco.doctor_id
join t_doctor_office_treatment as dot on dot.doctor_office_id = dco.id
join t_doctor_office_treatment_price as dotp on dotp.doctor_office_treatment_id = dot.id 


-- Query Location Praktek Baru
select distinct bio.fullname as NamaDokter, medfac.name as NamaRS, medfac.full_address as Alamat,
medfacat.name as NamaPoli, medsche.day as NamaJadwal, medsche.time_schedule_start as Mulai, 
medsche.time_schedule_end as Selesai, dotp.price_start_from as HargaMulai
from t_doctor_office_schedule as docsche
join m_doctor as doc on docsche.doctor_id = doc.id
join m_biodata as bio on doc.biodata_id = bio.id
join m_medical_facility_schedule as medsche on docsche.medical_facility_schedule_id = medsche.id
join m_medical_facility as medfac on medsche.medical_facility_id = medfac.id
join m_medical_facility_category as medfacat on medfac.medical_facility_category_id = medfac.id
join t_doctor_office as dco on doc.id = dco.doctor_id
join t_doctor_office_treatment as dot on dot.doctor_office_id = dco.id
join t_doctor_office_treatment_price as dotp on dotp.doctor_office_treatment_id = dot.id 


-- Query Location Praktek Baru
select bio.fullname as NamaDokter, medfac.name as NamaRS, medfac.full_address as Alamat,
medfacat.name as NamaPoli, medsche.day as NamaJadwal, medsche.time_schedule_start as Mulai, 
medsche.time_schedule_end as Selesai, dotp.price_start_from as HargaMulai
from t_doctor_office_schedule as docsche
join m_doctor as doc on docsche.doctor_id = doc.id
join m_biodata as bio on doc.biodata_id = bio.id
join m_medical_facility_schedule as medsche on docsche.medical_facility_schedule_id = medsche.id
join m_medical_facility as medfac on medsche.medical_facility_id = medfac.id
join m_medical_facility_category as medfacat on medfac.medical_facility_category_id = medfacat.id
join t_doctor_office as dco on doc.id = dco.doctor_id
join t_doctor_office_treatment as dot on dot.doctor_office_id = dco.id
join t_doctor_office_treatment_price as dotp on dotp.doctor_office_treatment_id = dot.id 



select * from m_medical_facility_schedule
select * from m_medical_facility
select * from m_medical_facility_category
select * from t_doctor_office_schedule
select * from m_doctor
select * from m_biodata

select * from t_doctor_office_treatment
select * from t_doctor_office_treatment_price
select * from t_doctor_treatment

select * from t_doctor_office_treatment_price
select * from t_doctor_office_treatment

truncate table t_doctor_office_treatment_price
insert into t_doctor_office_treatment_price values
(1, 30000, 300000, 1000000, 1,  getdate(), null, null, null, null, 0),
(2, 30000, 300000, 1000000, 1,  getdate(), null, null, null, null, 0),
(3, 30000, 300000, 1000000, 1,  getdate(), null, null, null, null, 0)

select * from t_doctor_office_treatment
truncate table t_doctor_office_treatment
select * from t_doctor_office
insert into t_doctor_office_treatment values
(1, 1, 1,  getdate(), null, null, null, null, 0),
(8, 4, 1,  getdate(), null, null, null, null, 0),
(13, 7, 1,  getdate(), null, null, null, null, 0)

-- Isi table
--Table Treatmen Doctor
select * from t_doctor_treatment
Insert into t_doctor_treatment values
(1, 'Fisioterapi Anak',1, GETDATE(), null, null, null, null, 0),
(1, 'Konsultasi Kesehatan Anak',1, GETDATE(), null, null, null, null, 0),
(1, 'Skrining Tumbuh Kembang Anak',1, GETDATE(), null, null, null, null, 0),
(1, 'Vaksin Anak',1, GETDATE(), null, null, null, null, 0),
(1, 'Konsultasi Alergi Anak',1, GETDATE(), null, null, null, null, 0),
(1, 'Konsultasi Psikologi Anak',1, GETDATE(), null, null, null, null, 0),
(1, 'Tes Pendengaran OAE',1, GETDATE(), null, null, null, null, 0),
(2, 'Ahli Gizi',1, GETDATE(), null, null, null, null, 0),
(2, 'Ahli Diet',1, GETDATE(), null, null, null, null, 0),
(2, 'Ahli Pola Hidup Sehat',1, GETDATE(), null, null, null, null, 0),
(2, 'Ahli Kecantikan',1, GETDATE(), null, null, null, null, 0),
(2, 'Ahli Makanan',1, GETDATE(), null, null, null, null, 0),
(3, 'Ahli Farmasi Obat',1, GETDATE(), null, null, null, null, 0),
(3, 'Spesialis Obat',1, GETDATE(), null, null, null, null, 0),
(3, 'Spesialis Penyakit Dalam',1, GETDATE(), null, null, null, null, 0)
